---
title: About
subtitle: Patrick Holthaus
save_as: index.html
documentclass: scrartcl
header-includes: \usepackage{noto}
status: hidden
template: about
---

**Patrick** is a Senior Research Fellow in the [Robotics Research Group](http://robotics.herts.ac.uk/) at the [University of Hertfordshire](http://www.herts.ac.uk/) (UK). He is also a supervisor of several PhD students and a Visiting Lecturer at the [School of Physics, Engineering and Computer Science](https://www.herts.ac.uk/study/schools-of-study/physics-engineering-and-computer-science), teaching foundational and advanced topics in computer science.

His research revolves around social robotics and focuses on nonverbal interactive signals, social credibility and trust in assistive and companion robots.
He is further interested in interaction architectures and behaviour coordination as well as systems integration in heterogeneous environments.
Patrick has extensive expertise in social human-robot interaction and experimentation and is highly skilled with a large array of robotic and sensing technologies.
As manager of the [Robot House](https://robothouse.herts.ac.uk/), a unique facility for human-robot interaction, he brings together real-world applications and fundamental robotics research. His research involving the [Kaspar](https://kaspar.herts.ac.uk/) robot has a direct impact on the life and learning of children with autism and learning difficulties.

<div class="row">
  <div class="col-xs-12 col-sm-6">
    <div class="img-thumbnail" style="margin: 10px 0 10px 0;">
      <div class="menubanner container-hover">
        <a style="border:none;" href="curriculum-vitae/">
          <div class="menuover">Curriculum Vitae</div>
          <img src="images/cv.webp">
        </a>
      </div>
    </div>
    <p></p>
  </div>
  <div class="col-xs-12 col-sm-6">
    <div class="img-thumbnail" style="margin: 10px 0 10px 0;">
      <div class="menubanner container-hover">
        <a style="border:none;" href="publications/">
          <div class="menuover">Publications</div>
          <img src="images/publications.webp">
        </a>
      </div>
    </div>
    <p></p>
  </div>
</div>

<!--Patrick is currently a CoI of the EPSRC Network+ project [Tackling Frailty - Facilitating the Emergence of Healthcare Robots from Labs into Service](https://gow.epsrc.ukri.org/NGBOViewGrant.aspx?GrantRef=EP/W000741/1) (EMERGENCE) and an advisory board member of the Norwegian innovation project [Human Interactive Robotics in Healthcare](https://ife.no/en/project/human-interactive-robotics-for-healthcare-hiro/) (HIRo).
Previously, he has been a CoI of the UKRI [TAS hub](https://www.tas.ac.uk/)'s pump priming project [Kaspar explains](https://www.tas.ac.uk/current-research-projects/kaspar-explains/) and the [Assuring Autonomy International Programme](https://www.york.ac.uk/assuring-autonomy/) (AAIP) funded feasibility project [Assuring safety and social credibility](https://www.york.ac.uk/assuring-autonomy/projects/assuring-safety-social-credibility/).
He has been a postdoctoral researcher in the [Robot House 2.0](https://gow.epsrc.ukri.org/NGBOViewGrant.aspx?GrantRef=EP/P020577/1) project, an EPSRC strategic equipment grant and the [Cognitive service robotics apartment](https://cit-ec.de/en/csra), a large-scale project within the DFG-funded cluster of excellence [Cognitive Interaction Technology](https://cit-ec.de) (CITEC) where he was a member of the [Cognitive systems engineering](https://cit-ec.de/en/cse/) group.-->

<!--Patrick is involved in the organisation of various international conferences and workshops as well as public scientific dissemination events.
He has been general chair of [UKRAS'21](https://www.ukras.org/news-and-events/uk-ras/) and publication chair of [HAI'17](https://hai-conference.net/hai2017/), the organiser of workshops at [ICMI'16](https://icmi.acm.org/2016/index.php?id=workshops), [HAI'17](https://cit-ec.de/en/events/workshop-multicentric-interaction-smart-homes-home-robots), and [RO-MAN'18-22](https://scrita.herts.ac.uk/) and special sessions at [RO-MAN'21-23](http://ro-man2023.org).
He is an associate editor of the [International Journal of Social Robotics](https://www.springer.com/journal/12369), and [Interaction Studies](https://benjamins.com/catalog/is), and an associate topic editor for human-robot/machine interaction at the [International Journal of Advanced Robotic Systems](https://journals.sagepub.com/home/arx).
Patrick has further edited a number of special issues in the International Journal of Social Robotics, [Paladyn, Journal of Behavioral Robotics](https://www.degruyter.com/journal/key/pjbr/12/1/html#issue-subject-group-specialissueontrust,acceptanceandsocialcuesinhumanrobotinteraction) as well as [Interaction Studies](https://benjamins.com/catalog/is.20.3).
Moreover, he is an active reviewer for several other social robotics conferences and journals.

Patrick received his PhD in 2015 on the topic of an [Integrated concept of spatial awareness](https://pub.uni-bielefeld.de/publication/2733038).
His thesis originates from research conducted in the [Applied Informatics Group](https://aiweb.techfak.uni-bielefeld.de) and SFB 673 [Alignment in Communication](http://sfb673.org) at Bielefeld University.
Thereby, he has been a named researcher in the *Cognitive Robotics* part, developing the [Receptionist scenario](https://aiweb.techfak.uni-bielefeld.de/content/receptionist-scenario), as a member of the project [Interaction space](https://aiweb.techfak.uni-bielefeld.de/content/c1-interaction-space) (C1).
His research within the project has been focused on human-robot interaction, particularly social communication signs in the spatial dimension.
Patrick also received master of science and bachelor of science degrees in computer science from Bielefeld University in 2009 and 2006, respectively. The topic of his master thesis was *Proxemics for a Social Robot*.-->
