##!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals
from markdown import Markdown
import re

### METADATA
##########

AUTHOR = u'Patrick Holthaus'
SITENAME = u'Patrick Holthaus'
SITETITLE = u'Social Robotics and Cognitive Interaction Technologies'
BANNER_SUBTITLE= u'Social Robotics and Cognitive Interaction Technologies'

TIMEZONE = 'Europe/London'
DEFAULT_LANG = 'en'

### PATH SETTINGS (relative)
##########

# pages and articles
PATH = 'content'
ARTICLE_PATHS = [ 'blog' ]

# templates (in addition to theme templates) and plugins
PLUGIN_PATHS = [ 'pelican-plugins', 'plugins' ]

# things to copy
STATIC_PATHS = [
  'images',
  'extra',
  'publications'
]

# Tell Pelican to change the path to 'static/custom.css' in the output dir
EXTRA_PATH_METADATA = {
  'extra/custom.css': {'path': 'static/css/custom.css'},
  'extra/custom.js': {'path': 'static/js/custom.js'},
  'extra/cookies-eu-banner.min.js': {'path': 'static/js/cookies-eu-banner.min.js'},
  'extra/robots.txt': {'path': 'robots.txt'}
}

CUSTOM_CSS = 'static/css/custom.css'
CUSTOM_JS = 'static/js/custom.js'

# Supress generation of blogging-related sites
ARCHIVES_SAVE_AS = ''
AUTHORS_SAVE_AS = ''
CATEGORIES_SAVE_AS = ''
TAGS_SAVE_AS = ''
INDEX_SAVE_AS = ''

### DISPLAY OPTIONS
##########

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Twitter button
# TWITTER_USERNAME = "_pholthaus"
# TWITTER_WIDGET_ID = "1"

# Blogroll
LINKS = [
  ('Robotics research group', 'http://robotics.herts.ac.uk/'),
  ('Robot house', 'https://robothouse.herts.ac.uk/'),
  ('Kaspar robot', 'https://kaspar.herts.ac.uk/')
]

# Social widget
SOCIAL = [
  ('Scholar', 'https://scholar.google.com/citations?user=kJ8R3eoAAAAJ', 'user'),
  ('ORCiD', 'https://orcid.org/0000-0001-8450-9362', 'user'),
  ('Twitter', 'https://twitter.com/_pholthaus'),
  ('Github', 'http://github.com/pholthaus'),
  ('Gitlab', 'http://gitlab.com/pholthaus'),
  ('E-mail', 'mailto:&#112;&#097;&#116;&#114;&#105;&#099;&#107;&#046;&#104;&#111;&#108;&#116;&#104;&#097;&#117;&#115;&#064;&#103;&#109;&#097;&#105;&#108;&#046;&#099;&#111;&#109;', 'envelope')
]

# Menu items
DISPLAY_PAGES_ON_MENU = True
PAGE_URL = '{slug}/'
PAGE_SAVE_AS = '{slug}/index.html'
# make article page a separate "blogs" page (our index is a static page)

#MENUITEMS = [
  #('Blog', '/blog/')
  #('Curriculum Vitae', '/curriculum-vitae/'),
  #('Publications', '/publications/')
#]

### THEME SETTINGS
##########
THEME = 'pelican-themes/pelican-bootstrap3'
BOOTSTRAP_THEME = 'simplex'
THEME_TEMPLATES_OVERRIDES = [ 'templates' ]
I18N_TEMPLATES_LANG = 'en'
#FAVICON = ''
FAVICON = 'images/signal-agents512.webp'
#SITELOGO = ''
#SITELOGO_SIZE = ''
AVATAR = 'images/avatar2.webp'
#ABOUT_ME = 'I am <a href="https://www.herts.ac.uk/study/schools-of-study/engineering-and-computer-science/research-in-engineering-and-computer-science/the-phd-programme-in-computer-science">advertising</a> the doctoral project<br><strong><a href="https://www.herts.ac.uk/__data/assets/pdf_file/0004/284359/Social-Credibility-of-Interactive-Robots.pdf">Social Credibility of Interactive Robots</a>!</strong>'

# RECENT_NEWS_HEADER = ''
# RECENT_NEWS = 'Submission open:\
# <ul>\
#   <li>\
#     <b>Special issue:</b><br>\
#     <a href="https://www.degruyter.com/publication/journal_key/PJBR/downloadAsset/PJBR_Curiosity%20in%20Human-Robot%20Interaction.pdf">Curiosity in Human-Robot Interaction</a><br>\
#     <a href="https://www.degruyter.com/journal/key/pjbr/html">Paladyn</a><br>\
#     Deadline: 19 Dec 2022<br>\
#   </li>\
# </ul>'

HIDE_SITENAME = False
DISPLAY_ARTICLE_INFO_ON_INDEX = False
BANNER = 'images/banner_robothouse.webp'
BANNER_ALL_PAGES = True
RESPONSIVE_IMAGES = True
JINJA_ENVIRONMENT = { 'extensions': ['jinja2.ext.i18n'] }
#DEFAULT_PAGINATION = False
# i18n needed for bootstrap3 theme
PLUGINS = [ 'i18n_subsites' ]
#SIDEBAR_IMAGES_HEADER = ''
#SIDEBAR_IMAGES = ["",""]

### PLUGIN SETTINGS
PLUGINS.append('cv')

# sitemap
PLUGINS.append('sitemap')
SITEMAP= {'format': 'xml',
          'exclude': 'about/'}

# bibtex files
PLUGINS.append('pelican-bibtex')
PUBLICATIONS_NAVBAR = True
PUBLICATIONS = {
  'a-journals': {
    'title': 'Journals',
    'file': 'journals.bib',
    'split': False,
    'highlight': ['Patrick Holthaus'],
    'group_type': False},
  'b-conferences': {
    'title': 'Conferences',
    'file': 'conferences.bib',
    'split': True,
    'highlight': ['Patrick Holthaus'],
    'group_type': False},
  'c-workshops': {
    'title': 'Workshops',
    'file': 'workshops.bib',
    'split': False,
    'highlight': ['Patrick Holthaus'] },
  'd-others': {
    'title': 'Others',
    'file': 'others.bib',
    'split': False,
    'bottom_link': False,
    'highlight': ['Patrick Holthaus'] }
}
# markdown processor for jinja
markdown = Markdown(extensions=['markdown.extensions.extra'])
def md(content, *args):
    p = '<p>'
    np = '</p>'
    md = markdown.convert(content)
    if md.startswith(p) and md.endswith(np): #you filthy bastard
      md = md[len(p):-len(np)]
    return md

def clean(content, *args):
    return re.sub(r'\W+', '', content)

JINJA_FILTERS = {
    'md': md,
    'clean': clean
}

CV_CONTENTS = 'content/cv.yaml'

CV_PDF = True
CV_PDF_TEMPLATE = 'cv.tex'
CV_PDF_RESULT = 'curriculum-vitae/cv.pdf'
CV_ADDITIONAL_SOURCES = [ 'peer.bib', 'others.bib', 'journals.bib', 'conferences.bib', 'workshops.bib', 'letter.tex', 'plainyrrev.bst' ]
CV_PREAMBLE = 'letter.tex'
CV_NAVBAR = True

CV_METRICS = True
