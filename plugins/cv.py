
import logging
import collections
logger = logging.getLogger(__name__)

from pelican import signals

import os.path
import yaml
import pprint
import jinja2
import re


from subprocess import Popen
import os
import tempfile
import shutil

from scholarly import scholarly
from scholarly import ProxyGenerator
from datetime import date

def load_metrics(scholar_id):
    pg = ProxyGenerator()
    pg.FreeProxies()
    scholarly.use_proxy(pg)
    author = scholarly.search_author_id(scholar_id)
    details = scholarly.fill(author, sections=['indices', 'publications', 'coauthors'])
    return details

def latex_replace(text):
    """Convert markdown links to latex links"""
    if not text is None:
      text = re.sub("\[(.*?)\]\((.*?)\)", '\\\\href{\\2}{\\\\uline{\\1}}', text)
      text = re.sub("#","\#", text)
      text = re.sub("&","\&", text)
      text = re.sub("\$", "\\\$", text)
      return text


def compile_tex(rendered_tex, pdf_result_path, additional_sources):
    tmp_dir = tempfile.mkdtemp()
    in_tmp_path = os.path.join(tmp_dir, 'cv.tex')
    out_tmp_path = os.path.join(tmp_dir, 'cv.pdf')

    with open(in_tmp_path, 'w') as outfile:
        outfile.write(rendered_tex)

    for additional_source in additional_sources:
      if os.path.exists(additional_source):
        shutil.copy(additional_source, tmp_dir)

    cur_dir = os.getcwd()
    os.chdir(tmp_dir)

    p = Popen(['pdflatex', 'cv'])
    p.communicate()

    for fn in os.listdir(tmp_dir):
      if fn.endswith(".aux"):
        p = Popen(['bibtex', fn])
        p.communicate()

    p = Popen(['pdflatex', 'cv'])
    p.communicate()
    p = Popen(['pdflatex', 'cv'])
    p.communicate()

    os.chdir(cur_dir)

    os.makedirs(os.path.dirname(pdf_result_path), exist_ok=True)
    shutil.copy(out_tmp_path, pdf_result_path)
    shutil.rmtree(tmp_dir)

def render_tex(yaml_content, metrics_content, preamble_path, publication_content, source_content, template_path, template_file, jinja_filters):
  latex_jinja_env = jinja2.Environment(
    block_start_string = '\BLOCK{',
    block_end_string = '}',
    variable_start_string = '\VAR{',
    variable_end_string = '}',
    comment_start_string = '\#{',
    comment_end_string = '}',
    line_statement_prefix = '%%',
    line_comment_prefix = '%#',
    trim_blocks = True,
    autoescape = False,
    loader = jinja2.FileSystemLoader(template_path)
  )

  latex_jinja_env.filters["latex"] = latex_replace
  template = latex_jinja_env.get_template(template_file)
  rendered_tex = template.render(yaml=yaml_content,preamble=preamble_path,pubdata=publication_content,sources=source_content,metrics=metrics_content)
  return rendered_tex

def read_members(generators):
    generator=generators[0]

    if 'CV_CONTENTS' not in generator.settings:
      logger.error('`cv.py` failed to find CV_CONTENTS in configuration, aborting.')
      return

    try:
      yaml_file_path = generator.settings['CV_CONTENTS']
      yaml_file = open(yaml_file_path, 'r', encoding='utf8')
      yaml_content = yaml.safe_load(yaml_file)
      generator.context['yaml'] = yaml_content

    except Exception as inst:
      logger.error('`cv.py` failed to open yaml file \'%s\': Exception %s (%s)', yaml_file_path, type(inst), inst)
      return

    metrics_content = None
    if 'CV_METRICS' not in generator.settings:
      logger.warn('`cv.py` failed to find CV_METRICS in configuration, skipping.')
    else:
      if generator.settings['CV_METRICS']:
        try:
          metrics_content = load_metrics(yaml_content['meta']['scholar'])
          if metrics_content is not None:
            metrics_content['updated'] = date.today().strftime("%d %b, %Y")
            metrics_content['citations'] = metrics_content['citedby']
            metrics_content['numcooauthors'] = len(metrics_content['coauthors'])
            metrics_content['numpublications'] = len(metrics_content['publications'])
            generator.context['yaml']['meta']['metrics'] |= metrics_content

          else:
            logger.warn('`cv.py` failed to load scholar metrics %s: %s. Skipping...', yaml_content['meta']['scholar'], 'No data found')

        except Exception as inst:
          logger.warn('`cv.py` failed to load scholar metrics for ID \'%s\': Exception %s (%s). Skipping...', yaml_content['meta']['scholar'], type(inst), inst)
          metrics_content = None
      else:
        logger.warn('`cv.py`: CV_METRICS disabled, skipping.')
    


    if 'CV_PDF' not in generator.settings:
      logger.warn('`cv.py` failed to find CV_PDF in configuration, skipping.')
    else:
      if generator.settings['CV_PDF']:
        template_paths = os.path.join(generator.settings['PATH'] + "/../",
                                      *generator.settings['THEME_TEMPLATES_OVERRIDES'])
        pdf_result_path = os.path.join(generator.settings['OUTPUT_PATH'],
                                      generator.settings['CV_PDF_RESULT'])

        preamble_path = os.path.join(generator.settings['PATH'], generator.settings['CV_PREAMBLE'])

        additional_sources = [os.path.join(generator.settings['PATH'], add_source)
                              for add_source in generator.settings['CV_ADDITIONAL_SOURCES']]

        try:
          tex = render_tex(yaml_content,
                          metrics_content,
                          preamble_path,
                          generator.settings['PUBLICATIONS'],
                          generator.settings['CV_ADDITIONAL_SOURCES'],
                          template_paths,
                          generator.settings['CV_PDF_TEMPLATE'],
                          generator.settings['JINJA_FILTERS'])
        except Exception as inst:
          logger.warn('`cv.py` failed to render tex from yaml, aborting: %s.', inst)
          return

        try:
          compile_tex(tex, pdf_result_path, additional_sources)
          print("PDF file generated as: " + pdf_result_path)
        except Exception as inst:
          logger.warn('`cv.py` failed to compile pdf from tex, aborting: %s.', inst)
          return
      else:
        logger.warn('`cv.py`: CV_PDF disabled, skipping.')

def register():
  signals.all_generators_finalized.connect(read_members)
